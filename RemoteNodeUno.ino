#include <SoftwareSerial.h>
#include <PacketSerial.h>
#include <SimpleTimer.h>

const byte HC12RxdPin = 18;                      // "RXD" Pin on HC12
const byte HC12TxdPin = 19;                      // "TXD" Pin on HC12
//const byte HC12SetPin = 11;                      // "SET" Pin on HC12

// Software Serial ports Rx and Tx are opposite the HC12 Rx and Tx
// Create Software Serial Port for HC12
SoftwareSerial HC12(HC12TxdPin, HC12RxdPin);
PacketSerial packet_serial;

//  The ping timer object
SimpleTimer timer;

//  Node ping interval
#ifndef PING_INTERVAL // how often do I ping the home nodes (in ms)
#define PING_INTERVAL 5000
#endif

int dipPins[] = {10,11,12,13};       // DIP Switch Pins

//  packet layout
typedef struct {
	uint8_t nodeID;
	uint8_t bit[8];
} _packet;

void setup() {

  //HC12ReadBuffer.reserve(64);                   // Reserve 64 bytes for Serial message input
  //SerialReadBuffer.reserve(64);                 // Reserve 64 bytes for HC12 message input

  //pinMode(HC12SetPin, OUTPUT);                  // Output High for Transparent / Low for Command
  //digitalWrite(HC12SetPin, HIGH);               // Enter Transparent mode
  //delay(80);                                    // 80 ms delay before operation per datasheet
  Serial.begin(9600);                           // Open serial port to computer
  HC12.begin(9600);                             // Open software serial port to HC12
  packet_serial.setPacketHandler(&onPacket);
  packet_serial.begin(&HC12);
  
  for(int i=2;i<10;i++) {                        // 2-9 are our output pins
    pinMode(i, OUTPUT);
  }
  //  Rotary switch for node ID selection
  for(int i = 0; i<=3; i++){
    pinMode(dipPins[i], INPUT_PULLUP);      // set the digital pins (defined above) as input
  }
  //  input pins
  for(int i=14;i<=17;i++) {
    pinMode(i,INPUT_PULLUP);
  }
  delay(1000);
  Serial.print("Node ID: ");
  Serial.println(address());
  timer.setInterval(PING_INTERVAL, pingHomeNode);
  Serial.println("Ready...");
}

void onPacket(const uint8_t* buffer, size_t size)
{
  // Make a temporary buffer.
  uint8_t tmp[size]; 
  
  // Copy the packet into our temporary buffer.
  Serial.println("onPacket");
  memcpy(tmp, buffer, size); 
  int nodeID = address();
  Serial.print("Node ID: ");
  Serial.println(nodeID);
  //  find our data
  //  Take our node ID, multiply by 9, read the byte.  
  //  If that byte is our node ID, then the packet is good.
  //  take the next 8 bytes
  int packetNodeID = tmp[nodeID*9];
  if( packetNodeID != nodeID ) {
    return;
  }
  //  show the bits
  Serial.print(tmp[nodeID*9+1]);
  Serial.print(tmp[nodeID*9+2]);
  Serial.print(tmp[nodeID*9+3]);
  Serial.print(tmp[nodeID*9+4]);
  Serial.print(tmp[nodeID*9+5]);
  Serial.print(tmp[nodeID*9+6]);
  Serial.print(tmp[nodeID*9+7]);
  Serial.print(tmp[nodeID*9+8]);
  Serial.println("");
  //  Set the IO pins
  for( int i=1;i<9;i++ ) {
    Serial.print("Pin ");
    Serial.print(i+1);
    if( tmp[nodeID*9+i] == 1 ) {
      Serial.println(" to HIGH");
      digitalWrite(i+1, HIGH);            
    } else {
      Serial.println(" to LOW");
      digitalWrite(i+1, LOW);                        
    }  
  }
}

void loop() {
	packet_serial.update(); 
	//
	timer.run();
}

//Read state from DIP Switch (4 positions used)
byte address(){
  byte i,j=0;
  for(i=0; i<=3; i++){
    j = (j << 1) | !digitalRead(dipPins[i]);   // read each input pin
  }
  return j; //return address
}

void pingHomeNode() {
  /*  send packet to home node
  */  
  Serial.print("Node ID: ");
  Serial.println(address());  
  Serial.println(F("-HC12 ping"));

  uint8_t packetSize = sizeof(_packet);
  uint8_t *packet = calloc(1,packetSize);
   
  _packet workPacket;
  for(int i=0;i<=8;i++)
    workPacket.bit[i] = 0;
  workPacket.nodeID = address();
  
  for(int i=14;i<=17;i++) {
    workPacket.bit[i-14] = address();//digitalRead(i);
  }
  memcpy(packet,&workPacket,sizeof(workPacket));
  packet_serial.send(packet,packetSize);
  free(packet);
}

